const express = require('express')
const multer = require('multer')
const path = require('path')
const fs = require('fs')
fs.mkdirSync('uploads', {recursive: true})
fs.mkdirSync('/tmp/files', {recursive: true})
let app = express()
let upload = multer({dest: '/tmp/files'})

app.get(['/', '/upload'], (req, res) => {
	res.send(`
		<!doctype html>
		<form method="post" action="/upload" enctype="multipart/form-data">
			<input type="file" name="file">
			<button>upload</button>
		</form>
		`)
})

app.post('/upload', upload.single('file'), (req, res) => {
	fs.copyFileSync(req.file.path, path.join('uploads', req.file.originalname))
	console.log('uploaded', req.file.originalname)
	res.send('uploaded')
})

app.listen(8888, ()=>{
	console.log('listening on port :8888')
})
